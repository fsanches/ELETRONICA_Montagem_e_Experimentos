Ref.     | FICHA | TITULO
---------+-------+-----------------------------------------
VA001/97 | MA01  | ?
VA002/97 | MA02  | AMPLIFICADOR DE ÁUDIO DE 15W
VB002/97 | MB02  | CONTROLE DE TONS / SISTEMA ESTEREOFÔNICO
VA003/97 | MA03  | DETECTOR ACÚSTICO DE FALHA NA REDE DE ENERGIA
VA004/97 | MA04  | ?
VA005/97 | MA05  | MEDIDOR DE TEMPO DE REAÇÃO
VA006/97 | MA06  | CONTROLE PARA DUAS LÂMPADAS DE 12V
VB006/97 | MB06  | ALIMENTAÇÃO FIXA POSITIVA
VA007/97 | MA07  | COMPROVADOR DE PORTO PARALELO
VB008/97 | MB01  | REGULADOR ELETRÔNICO DE POTÊNCIA 
VA009/97 | MA09  | GERADOR DE TONS DE ÁUDIO
VA010/97 | MA10  | ?
VA011/97 | MA11  | LIGAÇÃO SEQÜENCIAL DE DIODOS LED
VA012/97 | MA12  | CONTROLE DIGITAL DE VOLUME
VB012/97 | MB05  | INTERFACE DE POTÊNCIA ISOLADA
VA013/97 | MA13  | VISUALIZADOR BCD/7 SEGMENTOS
VA014/97 | MA14  | LUZES DESLOCÁVEIS BIDIRECIONAIS
VB014/97 | MB07  | GERADOR DE AVISO ACÚSTICO
VA015/97 | MA15  | MINUTEIRO PROGRAMÁVEL
VA016/97 | MA16  | SEPARADOS ESTÉREO DE GRAVES E AGUDOS
VB016/97 | MB08  | AMPLIFICADOR DE AGUDOS
VA017/97 | MA17  | TERMÔMETRO LCD
VA018/97 | MA18  | GERADOR DE ONDA QUADRADA A CRISTAL
VB018/97 | MB09  | FUSÍVEL ELETRÔNICO DE CORRENTE CONTÍNUA
VA019/97 | MA19  | AMPLIFICADOR DE GRAVES
VA020/97 | MA20  | MEDIDOR DE CONTINUIDADE
VB020/97 | MB10  | REGULADOR DE LUZ POR TATO
VA021/97 | MA21  | CONTROLADOR DE PORTO PARALELO DE OITO CANAIS
VA022/97 | MA22  | SISTEMA DE LUZES DE QUATRO CANAIS
VB022/97 | MB11  | FREQÜENCÍMETRO DE ÁUDIO
VA023/97 | MA23  | BASE DE TEMPOS DE PRECISÃO
VA024/97 | MA24  | VOLTÍMETRO DIGITAL DE 200mV
VB024/97 | MB06  | INTERRUPTOR DE LUZ GRADUAL
VA025/97 | MA25  | CONDICIONADOR DE SINAIS
VA026/97 | MA26  | RETIFICADOR PARA POLIMETRO
VB026/97 | MB12  | AVISADOR DE LUZ ACESA
VA027/97 | MA27  | CONTROLE PARA CONVERTEDORES
VA028/97 | MA28  | CARREGADOR DE BATERIAS DE 6V
VB028/97 | MB13  | AMPLIFICADOR DE MICROFONE
VA029/97 | MA29  | GERADOR DE ULTRA-SOM
VA030/97 | MA30  | FILTRO DE RECORTE DE GRAVES ESTÉREO
VB030/97 | MB14  | FILTRO DE RECORTE DE AGUDOS ESTÉREO
VA031/97 | MA31  | COMPARADOR DE JANELA
VA032/97 | MA32  | INDICADOR TEMPORIZADO DE LUZ
VB032/97 | MB15  | LUZ INTERMITENTE DE 12 VOLTS
VA033/97 | MA33  | TEMPORIZADOR DE POTÊNCIA A 12V
VA034/97 | MA34  | BALIZA COM LÂMPADA DE XENÔNIO
VB034/97 | MB16  | SONDA DE TEMPERATURA PARA POLÍMETRO
VA036/97 | MA36  | COMUTADOR DE ÁUDIO DE 4 CANAIS 
VB036/97 | MB17  | MÓDULO DE ALIMENTAÇÃO DE 13,5 V 15V
VA037/97 | MA37  | COMUTADOR LÂMPADA DE RESERVA
VA039/97 | MA39  | TEMPORIZADOR PARA LUZ DE ESCADA
VA042/97 | MA42  | VARIADOR LINEAR DE TENSÃO DIGITAL
VB042/97 | MB20  | AMPLIFICADOR DE ÁUDIO
VA043/97 | MA43  | MESCLADOR PARA MICROFONE E LINHA
VA045/97 | MA45  | PRÉVIO PARA GUITARRA ELÉTRICA
VA046/97 | MA46  | SEMÁFORO PARA TREM ELÉTRICO
VB046/97 | MB22  | LUZ TRASEIRA AUTOMATICA
VA047/97 | MA47  | EQUALIZADOR PARA GUITARRA
VA049/97 | MA49  | MINUTERIA TELEFÔNICA
VA050/97 | MA50  | CIRCUITO DE PRESENÇA PARA GUITARRA
VB050/97 | MB24  | CIRCUITO DE DISTORÇÃO
VA051/97 | MA51  | EMISSOR TELECOMANDO POR INFRAVERMELHOS
